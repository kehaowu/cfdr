#conditional False Discovery Rate (cFDR)

## 安装条件
*
*Linux 操作系统：** 本代码包安装环境为Linux x86_64，推荐使用[Ubuntu](http://www.ubuntu.org.cn/download)或者[CentOS](https://www.centos.org/)。

**R语言：** 本代码包会调用R语言，[R语言官网](https://www.r-project.org/)。

**git：***

## 安装

    # 下载代码包
    git clone https://bitbucket.org/kehaowu/cfdr.git
    cd cfdr

## 准备文件：以weight和height表型为例

>表型weight，文件名weight.txt，保存在raw文件夹内
    rs1 0.803
    rs2 0.312
    rs3 0.031

>表型height，文件名height.txt，保存在raw文件夹内
    rs1 0.234
    rs2 0.013
    rs3 0.004

## 计算

### 参数
 - **--trait1=** 表型一，比如height表型文件存储在height.txt文件中，则只需要输入height
 - **--trait2=** 表型二，输入方法同trait1
 - **--snp=all|intergenic** 用于计算cFDR的SNP，all为用所有prune后的SNP进行计算，intergenic表示仅用intergenic进行计算，用intergenic的SNP进行计算结果相对会更加保守。
 - **--prune=freq|pvalue|anno** SNP prune的方法，用freq表示当一对SNP对应的R2值大于0.2时删除MAF值较小的；用pvalue表示在类似的情况下删除p值分位数较大的；用anno表示在此情况下删除功能作用较弱的。
 - **--gc=false|true** 表示在计算cFDR之前是否还需要进行genomic control，true表示需要，false表示不需要。
 - **--run=all|prune|cFDR** 表示计算步骤，all表示从头计算；prune表示从prune步骤开始计算，前提是之前已经从头计算过；cFDR表示仅重新计算cFDR。