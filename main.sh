#!/bin/bash

## Predefine variables
RUN="all"
GC="false"
PRUNE="normal"
SNP="all"
TRAIT1="NO"
TRAIT2="NO"

. ./src/predefine.sh

for i in "$@"
do
    case $i in
        --trait1=*)
        TRAIT1="${i#*=}"
        shift 
        ;;
        --trait2=*)
        TRAIT2="${i#*=}"
        shift 
        ;;
        --gc=*)
        GC="${i#*=}"
        shift 
        ;;
        --snp=*)
        SNP="${i#*=}"
        shift 
        ;;
        --prune=*)
        PRUNE="${i#*=}"
        shift 
        ;;
        --run=*)
        RUN="${i#*=}"
        shift 
        ;;
        *)
                # unknown option
        ;;
    esac
done

usage () {
  warn "\t sh main.sh --trait1= --trait2= [--snp=all|intergenic] [--prune=freq|pvalue|anno] [--gc=false|true] [--run=all|prune|cFDR]"
}

init () {
  tell "Init..."
  sh ./src/initial.sh
  tell "Clean data folder"
  rm -rfv ./data/*$TRAIT1-$TRAIT2*
}

load () {
  tell "Loading data for $TRAIT1 and $TRAIT2 ..."
  Rscript ./src/combine.R $TRAIT1 $TRAIT2
}

prune () {
  tell "Pruning SNPs for $TRAIT1 and $TRIAT2 ..."
  sh ./src/prune.sh $TRAIT1 $TRAIT2 $PRUNE
}

annotate () {
  tell "Annotate SNPs for $TRAIT1 and $TRAIT2 ..."
  sh ./src/annotate.sh $TRAIT1 $TRAIT2 $PRUNE
}

calcFDR () {
  tell "Calculating cFDR for $TRAIT1 and $TRAIT2 ..."
  Rscript ./src/initialR.R
  Rscript ./src/cFDR.R $TRAIT1 $TRAIT2 $PRUNE $SNP $GC
}

summary () {
  tell "Ploting for $TRATI1 $TRAIT2 ..."
  Rscript ./src/summary.R $TRAIT1 $TRAIT2 $PRUNE $SNP $GC
}

runAll () {
  init
  load
  prune
  annotate
  calcFDR
  summary
  tell Completed at `date`
}

runPrune () {
  prune
  annotate
  calcFDR
  summary
  tell Completed at `date`
}

runcFDR () {
  calcFDR
  summary
  tell Completed at `date`
}

runSummary () {
  summary
  tell Completed at `date`
}

if [ $TRAIT1 == "NO" ]; then
  warn Trait 1 is required.
  usage
  exit
fi

if [ $TRAIT2 == "NO" ]; then
  warn Trait 2 is required.
  usage
  exit
fi

if [ $SNP != "all" -a $SNP != "intergenic" ]; then
  warn --snp=all or --snp=intergenic
  exit
fi

if [ $PRUNE != "normal" -a $PRUNE != "pvalue" -a $PRUNE != "anno" ]; then
  warn --prune=normal or --prune=pvalue or --prune=anno
  exit
fi

if [ $GC != "false" -a $GC != "true" ]; then
  warn --gc=false or --gc=true
  exit
fi

if [ $RUN != "all" -a $RUN != "cFDR" -a $RUN != "prune" -a $RUN != "summary" ]; then
  warn --run=all or --run=cFDR
  exit
fi

case $RUN in 
  "all")
    runAll
    exit
    ;;
  "cFDR")
    runcFDR
    exit
    ;;
  "prune")
    runPrune
    exit
    ;;
  "summary")
    runSummary
    exit
    ;;
esac

