#!/bin/bash

annovarpath="sys/annovar"

if [ ! -f ${annovarpath}/humandb/hg19_snp138.txt ]; then
	sh src/initialannovar.sh
fi

outpath="data"

echo "Begin to convert snplist to annovar format."
./${annovarpath}/convert2annovar.pl \
  -format rsid ${outpath}/pruned.$1-$2.$3.prune.in \
  -dbsnpfile ${annovarpath}/humandb/hg19_snp138.txt \
  > ${outpath}/4annovar.$1-$2.$3.pruned.snplist

./${annovarpath}/annotate_variation.pl \
  -out ${outpath}/annovared.$1-$2.$3.pruned.snplist \
  -build hg19 ${outpath}/4annovar.$1-$2.$3.pruned.snplist \
  sys/annovar/humandb/

echo "Extract intergenic SNPs."
grep intergenic \
  ${outpath}/annovared.$1-$2.$3.pruned.snplist.variant_function \
  > ${outpath}/annovared.$1-$2.$3.pruned.snplist.intergenic

echo "Obtain a rs_id"
awk {'print $8'} \
  ${outpath}/annovared.$1-$2.$3.pruned.snplist.intergenic \
  >  ${outpath}/annovared.$1-$2.$3.pruned.snplist.intergenic.rsid

