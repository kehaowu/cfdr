#!/bin/bash
. ./src/predefine.sh
if [ $# -ne 3 ]; then
  warn "Usage: sh prune.sh [file name 1] [file name 2] [normal|pvalue|anno]"
  exit
fi

vcfpath="sys/hapmap3"
outpath="data/"
popfile="src/pop/CEU_hapmap"
plinkpath="sys/plink"
annovarpath="sys/annovar"

if [ ! -f ${vcfpath}.ped ]; then
	sh src/downloadhapmap.sh
fi

if [ ! -f ${vcfpath}.map ]; then
	sh src/downloadhapmap.sh
fi

if [ ! -f ${plinkpath} ]; then
	cp src/plink sys/plink
fi

if [ ! -d ${outpath} ]; then
    mkdir -p ${outpath}
fi

./${plinkpath} --file ${vcfpath}\
    --extract ${outpath}combined.$1-$2.snplist \
    --keep ${popfile} \
    --make-bed \
    --out ${outpath}$1-$2.$3

./${plinkpath} --bfile ${outpath}$1-$2.$3\
    --write-snplist \
    --freq \
    --out ${outpath}$1-$2.$3

if [ $3 == "normal" ]; then
    tell Pruning SNPs using normal method [Removing minor MAF one].
elif [ $3 == "pvalue" ]; then
    tell Pruning SNPs using pvalue method [Removing major pvalue one].
elif [ $3 == "anno" ]; then
    tell Pruning SNPs using annotation method [Removing less important one].
    tell Begin to convert snplist to annovar format.
    ./${annovarpath}/convert2annovar.pl \
        -format rsid ${outpath}combined.$1-$2.snplist \
        -dbsnpfile ${annovarpath}/humandb/hg19_snp138.txt \
        > ${outpath}/4annovar.$1-$2.preprune.snplist
    tell Begin to annotate.
    ./${annovarpath}/annotate_variation.pl \
        -out ${outpath}/annovared.$1-$2.preprune.snplist \
        -build hg19 ${outpath}/4annovar.$1-$2.preprune.snplist \
        sys/annovar/humandb/
fi

Rscript src/pruneWeight.R $1 $2 $3

./${plinkpath} --bfile ${outpath}$1-$2.$3\
    --indep-pairwise 50 5 0.2 \
    --write-snplist \
    --read-freq ${outpath}$1-$2.$3.frq \
    --out ${outpath}pruned.$1-$2.$3

