#!/bin/bash

RED='\033[0;31m' # Red color
GREEN='\033[0;32m' # Green color
NC='\033[0m' # No Color

warn () {
  echo -e $RED $@ $NC
}

tell () {
  echo -e "\n$GREEN $@ $NC"
}
