#!/bin/bash

cp -vrf src/annovar sys/annovar

echo "Go to sys/annovar"
cd sys/annovar
./annotate_variation.pl -downdb -buildver hg19 -webfrom annovar refGene humandb/
./annotate_variation.pl -downdb -buildver hg19 -webfrom annovar snp138 humandb/
cd ../../
echo "Go back to main directory"
