#!/bin/bash

wget -O sys/bzip2-1.0.6.tar.gz http://www.bzip.org/1.0.6/bzip2-1.0.6.tar.gz
cd sys
tar -zxvf bzip2-1.0.6.tar.gz
cd bzip2-1.0.6
make
cd ../../
mv sys/bzip2-1.0.6/bzip2 sys/bzip2
rm -rf sys/bzip2-1.0.6
rm -rf sys/bzip2-1.0.6.tar.gz



