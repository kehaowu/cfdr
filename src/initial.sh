#!/bin/bash

if [ ! -d 'sys' ]; then
	mkdir sys
fi
if [ ! -d 'data' ]; then
	mkdir data
fi
if [ ! -d 'result' ]; then
	mkdir result
fi
if [ ! -d 'raw' ]; then
	mkdir raw
fi
