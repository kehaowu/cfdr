#!/bin/bash

# Download hapmap3 map file
if [ -f "sys/hapmap3.map.bz2" ]; then
	rm -v sys/hapmap3.map.bz2
fi
wget -O sys/hapmap3.map.bz2 ftp://ftp.ncbi.nlm.nih.gov/hapmap/genotypes/2009-01_phaseIII/plink_format/hapmap3_r2_b36_fwd.consensus.qc.poly.map.bz2


# Download hapmap3 ped file
if [ -f "sys/hapmap3.ped.bz2" ]; then
	rm -v sys/hapmap3.ped.bz2
fi
wget -O sys/hapmap3.ped.bz2 ftp://ftp.ncbi.nlm.nih.gov/hapmap/genotypes/2009-01_phaseIII/plink_format/hapmap3_r2_b36_fwd.consensus.qc.poly.ped.bz2


# Unzip map and ped files
if [ ! -f "sys/bzip2" ]; then
	sh src/installbzip2.sh
fi

./sys/bzip2 -vd sys/hapmap3.ped.bz2
./sys/bzip2 -vd sys/hapmap3.map.bz2



